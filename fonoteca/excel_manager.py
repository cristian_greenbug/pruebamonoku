from datetime import date, datetime

from django.contrib import messages
from django.utils.dateparse import parse_date
from numpy.core.defchararray import strip

from .models import *

class MessageError:
    def __init__(self):
        self._errors = {}

    @property
    def errors(self) -> dict:
        return self._errors

    def add_error(self, error: dict) -> None:
        self._errors.update(error)

    def __str__(self):
        # TODO: mostrar el detalle del error, solo muestra el key
        return str(self.errors)

    def __bool__(self):
        if self.errors:
            return True
        else:
            return False


class Manager:
    def __init__(self, value: str, errors: MessageError, **kwargs):
        self._errors = errors
        self._value = self.get_value(value, **kwargs) or None

    @property
    def value(self):
        return self._value

    @property
    def errors(self):
        return self._errors


class BandaManager(Manager):
    def get_value(self, value: str, **kwargs):
        try:
            banda, created = Banda.objects.get_or_create(nombre=value)

        except Banda.DoesNotExist:
            self.errors.add_error({'Banda': 'No se encontraron bandas con ese nombre'})
        else:
            return banda


class ArtistaManager(Manager):
    def get_value(self, value: str, **kwargs):
        try:
            artista, created = Artista.objects.get_or_create(nombre=value)
        except Artista.DoesNotExist:
            self.errors.add_error({'Banda': 'No se encontraron atistas con ese nombre'})
        else:
            return artista


class AlbumManager(Manager):
    def get_value(self, value: str, **kwargs):
        try:
            album, created = Album.objects.get_or_create(nombre=value,
                                                         fecha='2020-03-21',
                                                         artista=kwargs['artista'],
                                                         banda=kwargs['banda'])
        except Album.DoesNotExist:
            self.errors.add_error({'Banda': 'No se encontraron atistas con ese nombre'})
        else:
            return album


class GeneroManager(Manager):
    def get_value(self, value: str, **kwargs):
        try:
            genero, created = Genero.objects.get_or_create(nombre=value)
        except Genero.DoesNotExist:
            self.errors.add_error({'Banda': 'No se encontraron genero con ese nombre'})
        else:
            return genero


class SubgeneroManager(Manager):
    def get_value(self, value: str, **kwargs):
        try:
            subgenero, created = SubGenero.objects.get_or_create(nombre=value, genero=kwargs['genero'])
        except Subgenero.DoesNotExist:
            self.errors.add_error({'Banda': 'No se encontraron subgenero con ese nombre'})
        else:
            return subgenero


class CancionManager(Manager):
    def get_value(self, value: dict, **kwargs):
        try:
            cancion, created = Cancion.objects.get_or_create(nombre=value['NOMBRE'],
                                                             duracion=value['DURACION'],
                                                             id_externo=value['ID_EXTERNO'],
                                                             labels=value['LABELS'],
                                                             instrumentos=value['INSTRUMENTOS'],
                                                             genero=kwargs['genero'],
                                                             subgenero=kwargs['subgenero'],
                                                             album=kwargs['album'])
        except Cancion.DoesNotExist:
            self.errors.add_error({'Banda': 'No se encontraron subgenero con ese nombre'})
        else:
            return cancion


class ExcelRowManager:
    def __init__(self, row, index):
        self._errors = MessageError()
        self._row = row
        self._index = index + 2
        self._banda = None
        self._artista = None
        self._album = None
        self._genero = None
        self._subgenero = None
        self._cancion = None

    @property
    def row(self):
        return self._row

    @property
    def errors(self):
        return self._errors

    @property
    def banda(self):
        return self._banda

    @property
    def artista(self):
        return self._artista

    @property
    def album(self):
        return self._album

    @property
    def genero(self):
        return self._genero

    @property
    def subgenero(self):
        return self._subgenero

    @property
    def cancion(self):
        return self._cancion

    def create(self):
        self.fill()
        if self.errors:
            return [messages.ERROR, f'ERROR: En la fila {self._index}. \nErrores: {self.errors}']
        else:
            return [messages.SUCCESS, f'Fila {self._index} creada con éxito.']

    def get_banda(self):
        self._banda = BandaManager(self.row['BANDA'],
                                   self.errors).value

    def get_artista(self):
        self._artista = ArtistaManager(
            self.row['ARTISTA'],
            self.errors
        ).value

    def get_album(self):
        self._album = AlbumManager(
            self.row['ALBUM'],
            self.errors,
            artista=self.artista,
            banda=self.banda
        ).value

    def get_genero(self):
        self._genero = GeneroManager(
            self.row['GENERO'],
            self.errors
        ).value

    def get_subgenero(self):
        self._subgenero = SubgeneroManager(
            self.row['SUBGENERO'],
            self.errors,
            genero=self.genero
        ).value

    def get_cancion(self):
        self._authors = CancionManager(
            self.row,
            self.errors,
            album=self.album,
            genero=self.genero,
            subgenero=self.subgenero,
        )

    def fill(self):
        self.get_banda()
        self.get_artista()
        self.get_genero()
        if not self.errors:
            self.get_subgenero()
            self.get_album()
            self.get_cancion()
        return
