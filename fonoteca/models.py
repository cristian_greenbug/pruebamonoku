from django.db import models


class Banda(models.Model):
    nombre = models.CharField(max_length=200, null=False, blank=False, verbose_name='Nombre')
    bandassimilares = models.CharField(max_length=1000, null=True, blank=True)


class Artista(models.Model):
    nombre = models.CharField(max_length=200, null=False, blank=False, verbose_name='Nombre')


class Album(models.Model):
    nombre = models.CharField(max_length=200, null=False, blank=False, verbose_name='Nombre')
    fecha = models.DateTimeField(null=False, blank=False)
    artista = models.ForeignKey(Artista, null=False, blank=False, on_delete=models.CASCADE)
    banda = models.ForeignKey(Banda, null=False, blank=False, on_delete=models.CASCADE)


class Genero(models.Model):
    nombre = models.CharField(max_length=100, null=False, blank=False)


class SubGenero(models.Model):
    nombre = models.CharField(max_length=100, null=False, blank=False)
    genero = models.ForeignKey(Genero, null=False, blank=False, on_delete=models.CASCADE)


class Cancion(models.Model):
    album = models.ForeignKey(Album, null=False, blank=False, verbose_name='Album', on_delete=models.CASCADE)
    id_externo = models.IntegerField(null=False, blank=False)
    nombre = models.CharField(max_length=200, null=False, blank=False, verbose_name='Nombre')
    genero = models.ForeignKey(Genero, null=False, blank=False, on_delete=models.CASCADE)
    subgenero = models.ForeignKey(SubGenero, null=False, blank=False, on_delete=models.CASCADE)
    duracion = models.CharField(max_length=10, null=False, blank=False)
    labels = models.CharField(max_length=300, null=True, blank=True)
    instrumentos = models.CharField(max_length=300, null=True, blank=True)





