from django.apps import AppConfig


class FonotecaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fonoteca'
