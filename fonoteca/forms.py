from django.forms import forms
from django import forms


class ExcelForm(forms.Form):
    file = forms.FileField(required=True)
    start = forms.IntegerField()
    end = forms.IntegerField()

    class Meta:
        fields = ['file', 'start', 'end']

    def clean_file(self):
        file = self.cleaned_data.get('file', None)
        _, file_format = file.name.split('.')
        if file_format not in ['xls', 'xlsx']:
            raise forms.ValidationError('Sube un archivo excel, formatos disponibles: .xls, .xlsx')
        else:
            return file