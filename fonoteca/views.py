import pandas as pd

from django.contrib import messages
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, redirect

# Create your views here.
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView

from fonoteca.excel_manager import ExcelRowManager
from fonoteca.forms import ExcelForm
from fonoteca.models import *


class apiview(APIView):

    def get(self):
        return JsonResponse('sldfkjs')


class CrearArtistaAPIView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        if Artista.objects.filter(nombre=request.POST.get('nombre')):
            return JsonResponse({'payload':'Artista repetido'})
        else:
            artista = Artista(nombre=request.POST.get('nombre'))
            artista.save()
            return JsonResponse({'payload': 'Artista creado'})

class FiltarCancionAPIView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        return JsonResponse({'payload': list(Cancion.objects.filter(Q(genero__nombre=request.GET.get('genero') )| Q(subgenero__nombre=request.GET.get('subgenero') ))),
                             })



def excel_view(request):
    template_name = 'upload_excel.html'
    if request.method == 'POST':
        print("entro en post")
        form = ExcelForm(request.POST, request.FILES)
        if form.is_valid():
            start = form.cleaned_data.get('start')
            end = form.cleaned_data.get('end')
            dataframe = pd.read_excel(form.cleaned_data['file'], na_filter='')
            for index, row in dataframe[start: end].iterrows():
                manager = ExcelRowManager(row, index)
                response = manager.create()
                messages.add_message(request, *response)
            return redirect('/admin')
        else:
            print("error: ")
            for error in form.errors:
                print("error: ", error)

            messages.error(request, 'El formato no es válido')
            return redirect('/excel/upload')
    else:
        form = ExcelForm(initial={'start': 0, 'end': 100})
    return render(request, template_name, dict(form=form))