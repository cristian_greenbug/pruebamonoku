
from django.urls import path

from fonoteca.views import apiview, CrearArtistaAPIView, FiltarCancionAPIView

urls = [
    path('artista/create', CrearArtistaAPIView.as_view(), name='home'),
    path('cancion/filtrar', FiltarCancionAPIView.as_view(), name='filtrar'),

]