from django.contrib import admin
from .models import *

admin.site.register(Banda)
admin.site.register(Artista)
admin.site.register(Album)
admin.site.register(Genero)
admin.site.register(SubGenero)
admin.site.register(Cancion)
