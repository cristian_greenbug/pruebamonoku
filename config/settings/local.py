from .base import *

DEBUG = True


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME':'fonoteca',
        'USER': 'postgres',
        'PASSWORD': 'crifa',
        'HOST': 'localhost',
        'PORT': ''
       },
   }

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static')
]