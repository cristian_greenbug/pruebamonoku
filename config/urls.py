
from django.contrib import admin
from django.urls import path, include
from fonoteca.urls import urls
from fonoteca.views import excel_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(urls)),
    path('excel/upload', excel_view, name='excel'),
]
